# GoA 2018 Artwork
## Goat of Arms
* Typeface: Adobe Source Code Pro
* License: http://creativecommons.org/licenses/by-sa/4.0/
* Based on: https://commons.wikimedia.org/wiki/File:Coat_of_arms_of_the_Socialist_Republic_of_Romania.svg by Alex:D (Public Domain)

## goa_vectorized.svg
* Vectorized bitmap

